export default function ({ app, route, store, isDev }) {
  const version = route.query._storyblok || isDev ? 'draft' : 'published'

  if (process.server) {
    store.commit('settings/setCachedVersion', app.$storyapi.cacheVersion)
  }

  if (!store.state.settings.settings._uid) {
    return store.dispatch('settings/loadSettings', { version })
  }
}
