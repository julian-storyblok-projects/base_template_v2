export default {
  computed: {
    smSize () {
      if (this.blok.small_size) {
        return this.blok.small_size
      }
      return null
    },
    mdSize () {
      if (this.blok.medium_size) {
        return this.blok.medium_size
      }
      return null
    },
    lgSize () {
      if (this.blok.large_size) {
        return this.blok.large_size
      }
      return null
    }
  }
}
