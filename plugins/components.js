import Vue from 'vue'

Vue.component('grid', () => import('~/components/Storyblok/TheGrid/TheGrid.vue'))
Vue.component('page', () => import('~/components/Storyblok/ThePage/ThePage.vue'))
Vue.component('page-banner', () => import('~/components/Storyblok/ThePageBanner/ThePageBanner.vue'))
Vue.component('page-feature', () => import('~/components/Storyblok/ThePageFeature/ThePageFeature.vue'))
Vue.component('page-image-and-text', () => import('~/components/Storyblok/ThePageImageAndText/ThePageImageAndText.vue'))
Vue.component('page-image-and-text-body', () => import('~/components/Storyblok/ThePageImageAndText/ThePageImageAndTextBody.vue'))
Vue.component('page-section-title', () => import('~/components/Storyblok/ThePageSectionTitle/ThePageSectionTitle.vue'))
Vue.component('page-text', () => import('~/components/Storyblok/ThePageText/ThePageText.vue'))
Vue.component('page-title', () => import('~/components/Storyblok/ThePageTitle/ThePageTitle.vue'))
Vue.component('post-preview', () => import('~/components/Storyblok/ThePostPreview/ThePostPreview.vue'))
Vue.component('page-carousel', () => import('~/components/Storyblok/ThePageCarousel/ThePageCarousel.vue'))
Vue.component('page-carousel-item', () => import('~/components/Storyblok/ThePageCarousel/ThePageCarouselItem.vue'))
Vue.component('page-video', () => import('~/components/Storyblok/ThePageVideo/ThePageVideo.vue'))
