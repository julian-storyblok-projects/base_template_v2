/**
 * Define all links for main navigation and footer here
 * [
        { title: "About", icon: "mdi mdi-information-variant", link: "/about" },
        {
            title: "Example",
            icon: "mdi mdi-briefcase-outline",
            children: [
                {
                    title: "Blog",
                    icon: "mdi mdi-briefcase-outline",
                    link: "/example"
                }
            ]
        }
    ]
 */

export default {
  mainNavigation: [
    {
      title: 'Blog',
      icon: 'mdi mdi-view-dashboard',
      link: '/blog'
    },
    {
      title: 'Lorem',
      icon: 'mdi mdi-alert-circle-outline',
      children: [
        {
          title: 'Blog',
          icon: 'mdi mdi-briefcase-outline',
          link: '/blog'
        }
      ]
    }
  ],
  footer: [
    {
      title: 'Impressum',
      icon: 'question_answer',
      link: '/impressum'
    },
    {
      title: 'Datenschutz',
      icon: 'question_answer',
      link: '/datenschutz'
    }
  ]
}
