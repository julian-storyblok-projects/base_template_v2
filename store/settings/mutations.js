export default {
  /**
     * Sets settings
     * @arguments String
     */
  setSettings (state, payload) {
    state.settings = payload
  },

  /**
     * Sets cached version
     * @arguments String
     */
  setCachedVersion (state, payload) {
    state.cacheVersion = payload
  }
}
