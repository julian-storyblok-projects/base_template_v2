export default {
  loadSettings ({ commit }, context) {
    return this.$storyapi.get('cdn/stories/settings', {
      version: context.version
    }).then((res) => {
      commit('settings/setSettings', res.data.story.content, { root: true })
    })
  }
}
